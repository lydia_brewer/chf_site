# initialize the django code
print('Initializing Django...')
import sys, os
mydir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(mydir)
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CHF.settings")
django.setup()

# regular imports
from account import models as amod
from catalog2 import models as cmod
from manager import models as mmod
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
import datetime, random, sys

#print("You should not be running this.  No soup for you.")
#sys.exit(0)

#####################################
###   Create Permissions and Groups

print()
print('Creating permissions and groups...')

# delete the existing groups and permissions assigned to them
Group.objects.all().delete()  # this also deletes everything in the association table between groups and permissions

# I'm not creating any permissions because I'm going to use the default ones that Django creates
# They are automatically created for each model class.  For the account.User class, we get:
#    account.add_user
#    account.change_user
#    account.delete_user

# manager group (managers have all permissions)
group_administrator = Group()
group_administrator.name = 'Administrator'
group_administrator.save()
for p in Permission.objects.all():
  group_administrator.permissions.add(p)

# SalesRep group (sales reps can change only catalog2 items)
group_volunteer = Group()
group_volunteer.name = 'Volunteer'
group_volunteer.save()
for p in Permission.objects.filter(content_type__app_label='catalog2'):
  group_volunteer.permissions.add(p)

# customer group (customers have no permissions)
group_basic = Group()
group_basic.name = 'Basic User'
group_basic.save()




######################################
###   Users

print()
print('Creating users...')
users = []  # to save for use later

# delete the existing ones
amod.User.objects.all().delete()

# new ones
for i in range(1, 10):
  u = amod.User()
  u.username = 'user%i' % i
  u.first_name = 'First%i' % i
  u.last_name = 'Last%i' % i
  u.email = 'me%i@me.com' % i
  u.set_password('pass%i' % i)
  u.address = 'Address%i' % i
  u.address2 = 'Address%i' % i
  u.city = 'City%i' % i
  u.birth = datetime.datetime.now()
  u.phone_number = '555-555-000%i' % i
  u.is_active = True
  if i == 1:
    u.is_staff = True
    u.is_superuser = True
  u.save()
  if i == 2:
    u.groups.add(group_administrator)
  else:
      u.groups.add(group_basic)

  print(u)
  users.append(u)
  # assign user to some groups/permissions
print('user1, pass1 is the superuser.')


# print the permissions of user2 so we know what to use with @permission_required().  user2 is in the Manager group, which has every permission.
print()
for name in sorted(users[1].get_all_permissions()):
  print('Permission:', name)



#####################################
###   Products

# delete existing categories and products
cmod.Individual_Item.objects.all().delete()
cmod.Rentable_Item.objects.all().delete()
cmod.Bulk_Item.objects.all().delete()
cmod.Product.objects.all().delete()
cmod.Category.objects.all().delete()


###############################################
### Product categories
print()
print('Creating product categories...')

categories = []
for i in range(1,4):
    if i == 1:
        cat = cmod.Category()
        cat.name = 'Clothing'
        cat.save()
        print(cat)
    if i == 2:
        cat = cmod.Category()
        cat.name = 'Furniture'
        cat.save()
        print(cat)
    if i == 3:
        cat = cmod.Category()
        cat.name = 'Souvenirs'
        cat.save()
        print(cat)
    categories.append(cat)


print()
print('Creating products...')
print('Creating rentable products...')
products = []
rentals = []
for i in range(1,3):
  c = cmod.Rentable_Item()
  c.name = 'Rental%i' % i
  c.description = 'This is rental item %i.' % i
  c.image = 'http://static.independent.co.uk/s3fs-public/thumbnails/image/2013/01/24/12/v2-cute-cat-picture.jpg'
  c.price = '1.23'
  c.item_type = 'Rental'
  c.rented_now = False
  c.category = random.choice(categories)
  c.save()
  print(c)
  rentals.append(c)
  products.append(c)


print()
print('Creating individual products...')

### NO!  Products cannot be created because they don't really exist.  Never do this:
#p = cmod.Product()

# individual products
individuals = []
for i in range(1, 3):
    if i == 1:
        p = cmod.Individual_Item()
        p.name = 'Blue Outfit with Wig'
        p.description = 'Blue coat, trousers, shoes, tights, undershirt, and wig.  Gun not included'
        p.item_type = 'Individual'
        p.image = 'http://citadel.sjfc.edu/students/cep08849/e-port/20763.jpg'
        p.creator = random.choice(users)
        p.price = '120.22'
        p.category = categories[0]
        p.available = True
        p.save()
        print(p)
    if i == 2:
        p = cmod.Individual_Item()
        p.name = "Revolutionary Dress"
        p.description = "Long colonial-style dress.  Size 12"
        p.item_type = 'Individual'
        p.image = 'https://www.heritagecostumes.com/secure/images/products/3493.jpg'
        p.creator = random.choice(users)
        p.price = '178.12'
        p.category = categories[0]
        p.available = True
        p.save()
        print(p)


    individuals.append(p)
    products.append(p)

# bulk products
bulks = []
for i in range(1, 5):
    if i == 1:
        p = cmod.Bulk_Item()
        p.name = 'Miniature Cannon Paperweight'
        p.description = 'Six inch cannon replica perfect for holding down papers.'
        p.item_type = 'Bulk'
        p.image = 'http://vignette3.wikia.nocookie.net/dragonball/images/d/dc/Cannon-625x415.jpg/revision/latest?cb=20150430182251'
        p.quantity = random.randint(1, 100)
        p.price = '12.66'
        p.category = categories[2]
        p.save()
        print(p)
    if i ==2:
        p = cmod.Bulk_Item()
        p.name = 'Chair'
        p.description = 'These chairs were hand-crafted by our amazing artisans'
        p.item_type = 'Bulk'
        p.image = 'http://www.colonialfurniture.us/images/history/history-cupboard-2.png'
        p.quantity = 4
        p.price = '32.66'
        p.category = categories[1]
        p.save()
        print(p)
    if i == 3:
        p = cmod.Bulk_Item()
        p.name = 'Brown Coat'
        p.description = 'Long brown coat with brass buttons.'
        p.item_type = 'Bulk'
        p.image = 'https://www.heritagecostumes.com/secure/images/products/7470.jpg'
        p.quantity = random.randint(1, 100)
        p.price = '87.66'
        p.category = categories[0]
        p.save()
        print(p)


    bulks.append(p)
    products.append(p)

###############################################
### Product Images
pics = []
for i in range(1,9):
    pic = cmod.ProductImage()
    if i == 1:
        pic.image = 'http://brethrencoast.com/weapon/ship-cannon.jpg'
        print('image added' + pic.image)
        pic.product = bulks[0]
        print(pic.product)
    if i == 2:
        pic.image = 'http://azcapitoltimes.com/files/2012/12/cannon.jpg'
        pic.product = bulks[0]
    if i == 3:
        pic.image = 'https://www.heritagecostumes.com/secure/images/products/8026.jpg'
        pic.product = individuals[0]
    if i == 4:
        pic.image = 'http://images.buycostumes.com/mgen/merchandiser/little-colonial-miss-child-costume-bc-20877.jpg'
        pic.product = individuals[1]
    if i == 5:
        pic.image = 'https://s-media-cache-ak0.pinimg.com/736x/09/b7/59/09b7593934090a5033df2a8a4fcd0078.jpg'
        pic.product = bulks[2]
    if i == 6:
        pic.image = 'http://lib.store.yahoo.net/lib/yhst-43237354811846/colonial-wardrobe-boy.jpg'
        pic.product = bulks[2]
    if i == 7:
        pic.image = 'https://www.heritagecostumes.com/secure/images/products/6251.jpg'
        pic.product = bulks[2]
    if i == 8:
        pic.image = 'http://www.colonialfurniture.us/images/history/history-clock-case-3.png'
        pic.product = bulks[1]
    pic.save()
    pics.append(pic)
###############################################
###   Venues and events


    print("added pics to prods")

print()
print('Creating venues and events...')


# delete the existing ones
cmod.Area.objects.all().delete()
cmod.Event.objects.all().delete()
cmod.Venue.objects.all().delete()

# create the venues
venues = []  # to save for use later
for i in range(1, 10):
  o = cmod.Venue()
  o.name = 'Venue%i' % i
  o.address = 'Address %i' % i
  o.city = 'City%i' % i
  o.state = 'State%i' % i
  o.zip_code ='ZipCode%i' % i
  o.save()
  print(o)
  venues.append(o)

# create the events
for i in range(1, 10):
  o = cmod.Event()
  o.name = 'Event%i' % i
  o.description = 'Description of the event %i' % i
  o.start_date = datetime.datetime.now()
  o.end_date = datetime.datetime.now()
  o.venue = random.choice(venues)
  o.save()
  # add some areas to the event
  for j in range(1, 4):
    a = cmod.Area()
    a.name = 'Area%i' % j
    a.description = 'Description of the area %i' % j
    a.place_number = i*j
    a.event = o
    a.save()
  print(o)

################################################3
##### Sales
