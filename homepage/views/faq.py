from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from datetime import datetime
from .. import dmp_render, dmp_render_to_response

@view_function
def process_request(request):
    print("Hey World")

    resp = "test"
    #return resp
    template_vars = {
    'now': datetime.now().year,
    'poem': 'Write me a haiku!',
    'resp':resp,
  }
    return dmp_render_to_response(request, 'faq.html', template_vars)
