from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.core.mail import send_mail
from datetime import datetime

@view_function
def process_request(request):
    print("hey")

    if request.method == "POST":
        clientemail = request.POST.get('email')
        clientname = request.POST.get('name')
        clientmessage = request.POST.get('message')
        print('Your client, ', clientname, ' who has an email address of ',clientemail, ' sent you a message that says ', clientmessage, '.')
        content = "Message from " + clientname + ", whose email address is " + clientemail + ', said "' + clientmessage + '"'
        touser_content = "Thanks, " + clientname + "!  We received the message you sent us.  We will respond as soon as possible!"
        print(touser_content)
        print(clientemail)
        send_mail('We got your message', touser_content, settings.EMAIL_HOST_USER,
        [clientemail], fail_silently=False)
        send_mail('Contact Page Message', content, settings.EMAIL_HOST_USER,
        ['postmaster@mail.colonial-heritage-festival.info'], fail_silently=False)
    template_vars = {

  }

    return dmp_render_to_response(request, 'contact.html', template_vars)
