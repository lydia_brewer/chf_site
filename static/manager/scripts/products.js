$(function() {

  $('.delete_button').click(function(e) {
      e.preventDefault();
      var href = $(this).attr("href");
      $('#realdeletebutton').attr("href", href);
      $('#delete_modal').modal("show");
    });//click
  $('#realdeletebutton').click(function(e) {
      $('#delete_modal').modal("hide");
    });//click

  $('.picture_button').click(function(e) {
      ///e.preventDefault();
      var href = $(this).attr("href");
      $('#closepic').attr("src", href);
    });//click

  $('.update_button').click(function(e) {
    e.preventDefault();
    var idvalue = $(this).attr("data-id");
    $(this).siblings('.quantity').load('/catalog/products.quantityUpdate/' + idvalue);
  });//click




});//ready
