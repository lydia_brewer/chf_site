$(function() {
$('#id_creator').closest("tr").hide();
$('#id_status').closest("tr").hide();
$('#id_quantity').closest("tr").hide();
  $('#id_producttype').change(function() {
    var choice =  $("#id_producttype").val();
    if (choice == 'Individual'){
      $('#id_creator').closest("tr").show();
      $('#id_status').closest("tr").hide();
      $('#id_quantity').closest("tr").hide();

    } else if (choice == 'Bulk'){
      $('#id_creator').closest("tr").hide();
      $('#id_status').closest("tr").hide();
      $('#id_quantity').closest("tr").show();
    }
    else {
      $('#id_creator').closest("tr").hide();
      $('#id_status').closest("tr").show();
      $('#id_quantity').closest("tr").hide();
    }
    });
$('#id_producttype').change();


});//ready
