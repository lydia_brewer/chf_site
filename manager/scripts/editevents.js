$(function () {
  $('#id_start_date').datetimepicker({
      format : 'YYYY-MM-DD'
  });
  $('#id_end_date').datetimepicker({
      format : 'YYYY-MM-DD'
  });

  $('#createareabutton').click(function(e) {
      e.preventDefault();
      var idvalue = $(this).attr("data-id");
      $.loadmodal({
        ///url: '/account/login/',
        url: '/manager/createarea/' + idvalue,
        id: 'add_area_modal',
        title: 'Please enter the area information.',
      });
  })//click

  $('.delete_button').click(function(e) {
      e.preventDefault();
      var href = $(this).attr("href");
      $('#realdeletebutton').attr("href", href);
      $('#delete_modal').modal("show");
    });//click
  $('#realdeletebutton').click(function(e) {
      $('#delete_modal').modal("hide");
    });//click
});
