from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from account.models import User
from account import models as amod
from catalog2 import models as cmod
from django.forms.models import model_to_dict

@view_function
def process_request(request):
    if not (request.user.has_perm('catalog2.change_product') or request.user.has_perm('catalog2.change_rentable_item') or request.user.has_perm('catalog2.change_individual_item') or request.user.has_perm('catalog2.change_bulk_item')):
        if not (request.user.has_perm('catalog2.add_product') or request.user.has_perm('catalog2.delete_product') or request.user.has_perm('catalog2.add_individual_item') or request.user.has_perm('catalog2.delete_individual_item') or request.user.has_perm('catalog2.add_rentable_item') or request.user.has_perm('catalog2.delete_rentable_item') or request.user.has_perm('catalog2.add_bulk_item') or request.user.has_perm('catalog2.delete_bulk_item')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/products/')
    try:
        p = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/products/')



    if p.item_type == "Rental":
        if request.user.has_perm('catalog2.change_product') or request.user.has_perm('catalog2.change_rentable_item'):

            #process the form
            form = EditRentableForm(initial={
                'name': p.name,
                'description': p.description,
                'image': p.image,
                'price':p.price,
                })
            if request.method == 'POST':
                form = EditRentableForm(request.POST)

                print("testing form")
                if form.is_valid():
                    p.name = form.cleaned_data.get('name')
                    p.image = form.cleaned_data.get('image')
                    p.description = form.cleaned_data.get('description')
                    if form.cleaned_data.get('status') == "Rentable":
                        p.rented_now = False
                    else:
                        p.rented_now = True
                    p.price = form.cleaned_data.get('price')
                    p.save()
                    return HttpResponseRedirect('/manager/products/')
                        #save data and redirect
                else:
                    print("form failure")
                    print(form.errors)

                    return HttpResponseRedirect('/manager/products/')
        else:
            return HttpResponseRedirect('/manager/products/error/')
    elif p.item_type == "Bulk":
        form = EditBulkForm(initial={
            'name': p.name,
            'description': p.description,
            'image': p.image,
            'quantity': p.quantity,
            'price':p.price,
            })
        if request.method == 'POST':
            form = EditBulkForm(request.POST)

            print("testing form")
            if form.is_valid():
                p.name = form.cleaned_data.get('name')
                p.image = form.cleaned_data.get('image')
                p.description = form.cleaned_data.get('description')
                p.quantity = form.cleaned_data.get('quantity')
                p.price = form.cleaned_data.get('price')
                p.save()
                return HttpResponseRedirect('/manager/products/')
                #save data and redirect
            else:
                print("form failure")
                print(form.errors)

                return HttpResponseRedirect('/manager/products/')
    elif p.item_type == "Individual":
        form = EditIndividualForm(initial={
            'name': p.name,
            'description': p.description,
            'image': p.image,
            'creator': p.creator,
            'price':p.price,
        })
        if request.method == 'POST':
            form = EditIndividualForm(request.POST)

            print("testing form")
            if form.is_valid():
                p.name = form.cleaned_data.get('name')
                p.image = form.cleaned_data.get('image')
                p.description = form.cleaned_data.get('description')
                p.creator = form.cleaned_data.get('creator')
                p.price = form.cleaned_data.get('price')
                p.save()



                return HttpResponseRedirect('/manager/products/')
                #save data and redirect
            else:
                print("form failure")
                print(form.errors)

                return HttpResponseRedirect('/manager/products/')


    template_vars = {
                'form':form,
                'p':p,
          }
    return dmp_render_to_response(request, 'editproduct.html', template_vars)

        #show the edit form html


class EditRentableForm(forms.Form):
    RENTALCHOICES = (
            ('Rentable' , 'Rentable'),
            ('Not Rentable', 'Not Rentable'),
    )
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='*Description', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    image = forms.CharField(label='*Image', required=True, max_length=1000, widget = forms.TextInput(attrs={'class':'form-control'}))
    status = forms.ChoiceField(label="*Rental Status", required=False, widget=forms.Select(attrs={'class':'form-control'}), choices=RENTALCHOICES)
    price = forms.DecimalField(label="*Price", decimal_places=2, max_digits=10, required = True, widget = forms.TextInput(attrs={'class':'form-control'}))




    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            p2 = cmod.Product.objects.get(name=self.cleaned_data.get('name'))
            if p2.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This product name is already in use, please choose a different one.')
        except cmod.Product.DoesNotExist:
            pass #name is free
            return name
        return name

    def clean(self):
        return self.cleaned_data

class EditBulkForm(forms.Form):

    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='*Description', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    image = forms.CharField(label='*Image', required=True, max_length=1000, widget = forms.TextInput(attrs={'class':'form-control'}))
    quantity = forms.IntegerField(label="*Bulk Item Quantity", required=False, widget=forms.NumberInput(attrs={'class':'form-control'}))
    price = forms.DecimalField(label="*Price", decimal_places=2, max_digits=10, required = True, widget = forms.NumberInput(attrs={'class':'form-control'}))




    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            p2 = cmod.Product.objects.get(name=self.cleaned_data.get('name'))
            if p2.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This product name is already in use, please choose a different one.')
        except cmod.Product.DoesNotExist:
            pass #name is free
            return name
        return name

    def clean(self):
        return self.cleaned_data

class EditIndividualForm(forms.Form):
    RENTALCHOICES = (
            ('Rentable' , 'Rentable'),
            ('Not Rentable', 'Not Rentable'),
    )
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='*Description', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    image = forms.CharField(label='*Image', required=True, max_length=1000, widget = forms.TextInput(attrs={'class':'form-control'}))
    creator = forms.ModelChoiceField(label="*Individual Item Creator",queryset=amod.User.objects.order_by('last_name','first_name'), required=False)
    price = forms.DecimalField(label="*Price", decimal_places=2, max_digits=10, required = True, widget = forms.TextInput(attrs={'class':'form-control'}))




    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            p2 = cmod.Product.objects.get(name=self.cleaned_data.get('name'))
            if p2.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This product name is already in use, please choose a different one.')
        except cmod.Product.DoesNotExist:
            pass #name is free
            return name
        return name






    def clean(self):
        return self.cleaned_data
