from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from account import models as amod
import datetime

from django.http import HttpResponse



@view_function
def process_request(request):
    # ensure user is logged in
    if request.user.has_perm('account.add_user') or request.user.has_perm('account.change_user') or request.user.has_perm('account.delete_user'):

        users = amod.User.objects.all().order_by('last_name','first_name')

    # send users to users.html
        template_vars = {
                'users':users,


          }
        return dmp_render_to_response(request, 'users.html', template_vars)
    if not request.user.has_permission:
        return HttpResponseRedirect('/homepage/index/')
    else:
        return HttpResponseRedirect('/manager/index/')


@view_function
def delete(request):
    if request.user.has_perm('account.delete_user'):
        print(request.urlparams[0])
        try:
            user = amod.User.objects.get(id=request.urlparams[0])
        except amod.User.DoesNotExist:
            return HttpResponseRedirect('/manager/users/')
        print('time to delete')
        user.delete()

        return HttpResponseRedirect('/manager/users/')
    else:
        return HttpResponseRedirect('/manager/users/')
