from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from account.models import User
from django.contrib.auth import models as authmod
from catalog2 import models as cmod
from account import models as amod
#from bootstrap3_datetime.widgets import DateTimePicker
from django.forms.models import model_to_dict

@view_function
def process_request(request):
    if not request.user.has_perm('account.change_user'):
        if not (request.user.has_perm('account.add_user') or request.user.has_perm('account.delete_user')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/users/')
    try:
        u = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/manager/users/')
    message = "yes"
    if u.id == request.user.id:
        message = "no"

    #process the form
    form = EditForm(initial=model_to_dict(u))
    if request.method == 'POST':
        form = EditForm(request.POST)

        print("testing form")
        if form.is_valid():
            print("form success")
            userpermission = form.cleaned_data.get('permission')
            u2 = amod.User.objects.get(id=request.urlparams[0])
            u2.username = form.cleaned_data.get('username')
            u2.first_name = form.cleaned_data.get('first_name')
            u2.last_name = form.cleaned_data.get('last_name')
            print("stuff")
            u2.address1 = form.cleaned_data.get('address1')
            u2.address2 = form.cleaned_data.get('address2')
            u2.city = form.cleaned_data.get('city')
            print("is")
            u2.state = form.cleaned_data.get('state')
            u2.zipcode = form.cleaned_data.get('zipcode')
            u2.birth = form.cleaned_data.get('birth')
            print("being")
            u2.phone_number = form.cleaned_data.get('phone_number')
            u2.email = form.cleaned_data.get('email')
            print("changed")
            u2.groups.clear()
            u2.user_permissions.clear()
            for group in form.cleaned_data.get('groups'):
                u.groups.add(group)
            print(form.cleaned_data.get('groups'))
            for permission in form.cleaned_data['user_permissions']:
                u2.user_permissions.add(permission)
                u2.has_permission = True
            print(u2.get_all_permissions())
            print(u2.groups)
            u2.save()
            return HttpResponseRedirect('/manager/users/')
            #save data and redirect
        '''else:
            print("form failure")
            print(form.errors)

            return HttpResponseRedirect('/manager/users/')'''


    template_vars = {
            'form':form,
            'user':u,
            'message':message,

      }
    return dmp_render_to_response(request, 'editusers.html', template_vars)

    #show the edit form html


class EditForm(forms.Form):
    username = forms.CharField(label='Username', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    first_name = forms.CharField(label='First Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    last_name = forms.CharField(label='Last Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address1 = forms.CharField(label='Address 1', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address2 = forms.CharField(label='Address 2', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='City', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='State', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='Zipcode', required=False,max_length=10, widget = forms.TextInput(attrs={'class':'form-control'}))
    birth = forms.DateField(label="Birthdate", required=False,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    phone_number = forms.CharField(label='Phone Number', required=False,max_length=15, widget = forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label="Email Address", required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    groups = forms.ModelMultipleChoiceField(label="*Group",queryset=authmod.Group.objects.all(), required=True,widget=forms.CheckboxSelectMultiple)
    user_permissions = cmod.PermissionModelMultipleChoiceField(label="*Individual Permissions",queryset=authmod.Permission.objects.order_by('name'), required=False,widget=forms.CheckboxSelectMultiple)




    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            u3 = User.objects.get(username=self.cleaned_data.get('username'))
            if u3.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This username is already in use, please choose a different one.')
        except User.DoesNotExist:
            pass #username is free
            return username
        return username

    def clean(self):


        return self.cleaned_data
