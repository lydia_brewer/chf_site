from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from catalog2 import models as cmod
from account import models as amod
from catalog2.models import Individual_Item
from catalog2.models import Rentable_Item
from catalog2.models import Bulk_Item
#from bootstrap3_datetime.widgets import DateTimePicker


@view_function
def process_request(request):
    if not (request.user.has_perm('catalog2.add_product') or request.user.has_perm('catalog2.add_bulk_item') or request.user.has_perm('catalog2.add_individual_item') or request.user.has_perm('catalog2.add_rentable_item')):
        if not (request.user.has_perm('catalog2.change_product') or request.user.has_perm('catalog2.delete_product') or request.user.has_perm('catalog2.change_bulk_item') or request.user.has_perm('catalog2.delete_bulk_item') or request.user.has_perm('catalog2.change_individual_item') or request.user.has_perm('catalog2.delete_individual_item') or request.user.has_perm('catalog2.change_rentable_item') or request.user.has_perm('catalog2.delete_rentable_item')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/products/')
    #process the form
    form = CreateProductForm()
    if request.urlparams[0] == "error":
        error = "You cannot create an item of that type."
    else:
        error = ""

    if request.method == 'POST':
        form = CreateProductForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data.get('producttype'))
            producttype = form.cleaned_data.get('producttype')
            if producttype == "Bulk":
                if request.user.has_perm('catalog2.add_bulk_item') or request.user.has_perm('catalog2.add_product'):
                    p = Bulk_Item()
                    p.name = form.cleaned_data.get('name')
                    p.description = form.cleaned_data.get('description')
                    p.image = form.cleaned_data.get('image')
                    p.quantity = form.cleaned_data.get('quantity')
                    p.price = form.cleaned_data.get('price')
                    p.item_type = form.cleaned_data.get('producttype')
                    p.save()
                    return HttpResponseRedirect('/manager/products/')
                else:
                    print('initial failure')
                    return HttpResponseRedirect('/manager/createproduct/error/')
            elif producttype == "Individual":
                if request.user.has_perm('catalog2.add_individual_item') or request.user.has_perm('catalog2.add_product'):
                    p = Individual_Item()
                    p.name = form.cleaned_data.get('name')
                    p.description = form.cleaned_data.get('description')
                    p.image = form.cleaned_data.get('image')
                    p.creator = form.cleaned_data.get('creator')
                    p.price = form.cleaned_data.get('price')
                    p.item_type = form.cleaned_data.get('producttype')
                    p.available = True
                    p.save()
                    return HttpResponseRedirect('/manager/products/')
                else:
                    return HttpResponseRedirect('/manager/createproduct/error/')
            elif producttype == "Rental":
                if request.user.has_perm('catalog2.add_rentable_item') or request.user.has_perm('catalog2.add_product'):
                    p = Rentable_Item()
                    p.name = form.cleaned_data.get('name')
                    p.description = form.cleaned_data.get('description')
                    p.image = form.cleaned_data.get('image')
                    status = form.cleaned_data.get('status')
                    if status == "Rentable":
                        p.rented_now = False
                    else:
                        p.rented_now = True
                    p.price = form.cleaned_data.get('price')
                    p.item_type = form.cleaned_data.get('producttype')
                    p.save()
                    print('item saved')
                    return HttpResponseRedirect('/manager/products/')
                else:
                    print('ultimate failure')
                    return HttpResponseRedirect('/manager/createproduct/error/')
            '''p.item_type = form.cleaned_data.get('producttype')
            p.save()
            return HttpResponseRedirect('/manager/products/')'''
        else:
            return HttpResponseRedirect('/manager/createproduct/error')




    template_vars = {
        'form': form,
        'error': error,


  }
    return dmp_render_to_response(request, 'createproduct.html', template_vars)

class CreateProductForm(forms.Form):
    OPTIONS = (
            #(" ", " "),
            ("Bulk", "Bulk"),
            ("Individual", "Individual"),
            ("Rental", "Rental"),
            )
    RENTALCHOICES = (
            ('Rentable' , 'Rentable'),
            ('Not Rentable', 'Not Rentable'),
    )
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='Description', required=False, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    image = forms.CharField(label='*Image file', required=True, max_length=1000, widget = forms.TextInput(attrs={'class':'form-control'}))
    producttype = forms.ChoiceField(label="*Product Type",required=True, widget=forms.Select(attrs={'class':'form-control'}), choices=OPTIONS)
    status = forms.ChoiceField(label="Rental Status", required=False, widget=forms.Select(attrs={'class':'form-control'}), choices=RENTALCHOICES)
    creator = forms.ModelChoiceField(label="Individual Item Creator",queryset=amod.User.objects.order_by('last_name','first_name'), required=False, widget=forms.Select(attrs={'class':'form-control'}))
    quantity = forms.IntegerField(label="Bulk Item Quantity", required=False, widget=forms.NumberInput(attrs={'class':'form-control'}))
    price = forms.DecimalField(label="*Price", decimal_places=2, max_digits=10, required = True, widget=forms.NumberInput(attrs={'class':'form-control'}))


    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            p2 = cmod.Product.objects.get(name=self.cleaned_data.get('name'))
            raise forms.ValidationError('This product name is already in use, please choose a different one.')
        except cmod.Product.DoesNotExist:
            pass #username is free
            return name
        return name



    def clean(self):
        return self.cleaned_data
