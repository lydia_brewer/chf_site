from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from account.models import User
from bootstrap3_datetime.widgets import DateTimePicker
from django.contrib.auth import models as authmod
from catalog2 import models as cmod


@view_function
def process_request(request):
    if not request.user.has_perm('account.add_user'):
        if not (request.user.has_perm('account.change_user') or request.user.has_perm('account.delete_user')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/users/')
    #process the form
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            #create a user object
            userpermission = form.cleaned_data.get('permission')
            u = User()
            u.username = form.cleaned_data.get('username')
            u.first_name = form.cleaned_data.get('first_name')
            u.last_name = form.cleaned_data.get('last_name')
            u.set_password(form.cleaned_data.get('password1'))
            u.address1 = form.cleaned_data.get('address1')
            u.address2 = form.cleaned_data.get('address2')
            u.city = form.cleaned_data.get('city')
            u.state = form.cleaned_data.get('state')
            u.zipcode = form.cleaned_data.get('zipcode')
            u.birth = form.cleaned_data.get('birth')
            u.phone_number = form.cleaned_data.get('phone_number')
            u.email = form.cleaned_data.get('email')
            u.save()
            u.groups.add(form.cleaned_data.get('group'))
            print(form.cleaned_data.get('group'))
            for permission in form.cleaned_data['permissions']:
                u.user_permissions.add(permission)
                u.has_permission = True
            print(u.get_all_permissions())
            print(u.groups)
            u.save()
            # fill the user object with the data from the form

            return HttpResponseRedirect('/manager/users/')




    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'createuser.html', template_vars)

class CreateUserForm(forms.Form):
    username = forms.CharField(label='*Username', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    password1 = forms.CharField(label='*Password',required=True,max_length=100,widget = forms.TextInput(attrs={'class':'form-control'}))
    password2 = forms.CharField(label='*Confirm Password',required=True,max_length=100,widget = forms.TextInput(attrs={'class':'form-control'}))
    first_name = forms.CharField(label='*First Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    last_name = forms.CharField(label='*Last Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address1 = forms.CharField(label='Address 1', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address2 = forms.CharField(label='Address 2', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='City', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='State', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='Zipcode', required=False,max_length=10, widget = forms.TextInput(attrs={'class':'form-control'}))
    birth = forms.DateField(label="Birthdate", required=False,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    phone_number = forms.CharField(label='Phone Number', required=False,max_length=15, widget = forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label="Email Address", required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    group = forms.ModelChoiceField(label="*Individual Permissions",queryset=authmod.Group.objects.order_by('name'), required=False,widget=forms.RadioSelect)
    permissions = cmod.PermissionModelMultipleChoiceField(label="*Individual Permissions",queryset=authmod.Permission.objects.order_by('name'), required=False,widget=forms.CheckboxSelectMultiple)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            user = User.objects.get(username=self.cleaned_data.get('username'))
            raise forms.ValidationError('This username is already in use, please choose a different one.')
        except User.DoesNotExist:
            pass #username is free

        #or do it this way
    #    users = User.objects.filter(username=self.cleaned_data.get('username'))
    #    if len(users) > 0:
    #        raise forms.ValidationError('This username is already in use, please choose a different one.')

        return username
    def clean(self):
        if self.cleaned_data.get('password1') != self.cleaned_data.get('password2'):
            raise forms.ValidationError('The two passwords do not match.')
        return self.cleaned_data
