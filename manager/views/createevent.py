from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from catalog2.models import Venue
from catalog2.models import Event
from catalog2 import models as cmod
from bootstrap3_datetime.widgets import DateTimePicker
from django.forms import ModelChoiceField


@view_function
def process_request(request):
    if not request.user.has_perm('catalog2.add_event'):
        if not (request.user.has_perm('catalog2.change_event') or request.user.has_perm('catalog2.delete_event')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/events/')
    #process the form
    form = CreateEventForm()

    if request.method == 'POST':
        form = CreateEventForm(request.POST)
        if form.is_valid():
            #create a user object
            e = Event()
            e.name = form.cleaned_data.get('name')
            e.description = form.cleaned_data.get('description')
            e.start_date = form.cleaned_data.get('start_date')
            e.end_date = form.cleaned_data.get('end_date')
            e.venue = form.cleaned_data.get('venue')
            e.save()

            # fill the user object with the data from the form

            return HttpResponseRedirect('/manager/events/')




    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'createevent.html', template_vars)

class CreateEventForm(forms.Form):
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='Description', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    start_date = forms.DateField(label="*Start Date", required=True,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    end_date = forms.DateField(label="*End Date", required=True,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    venue = cmod.VenueModelChoiceField(label="*Venue",queryset=cmod.Venue.objects.order_by('name'), required=True, widget = forms.Select(attrs={'class':'form-control'}))


    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            event = Event.objects.get(name=self.cleaned_data.get('name'))
            raise forms.ValidationError('This event name is already in use, please choose a different one.')
        except Event.DoesNotExist:
            pass #username is free

        #or do it this way
    #    users = User.objects.filter(username=self.cleaned_data.get('username'))
    #    if len(users) > 0:
    #        raise forms.ValidationError('This username is already in use, please choose a different one.')

        return name
    def clean(self):
        return self.cleaned_data
