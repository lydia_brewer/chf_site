from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from catalog2.models import Area
from catalog2.models import Event
from catalog2 import models as cmod
from bootstrap3_datetime.widgets import DateTimePicker
from django.forms import ModelChoiceField


@view_function
def process_request(request):
    if not request.user.has_perm('catalog2.add_area'):
        error = "You don't have permission to do this!"
    else:
        error = ""
    #process the form
    form = CreateAreaForm()
    event_id=request.urlparams[0]

    if request.method == 'POST':
        form = CreateAreaForm(request.POST)
        print("Testing form")
        if form.is_valid():
            #create a user object
            a = Area()
            a.name = form.cleaned_data.get('name')
            a.description = form.cleaned_data.get('description')
            a.place_number = form.cleaned_data.get('place_number')
            a.event = cmod.Event.objects.get(id=request.urlparams[0])
            a.save()



            return HttpResponse('''
            <script>
                window.location.href = '/manager/editevents/' + event_id;
            </script>
            ''')
        else:
            print("failed")




    template_vars = {
        'form': form,
        'id':event_id,
        'error':error,

  }
    return dmp_render_to_response(request, 'createarea.html', template_vars)

class CreateAreaForm(forms.Form):
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='Description', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    place_number = forms.IntegerField(label="*Place Number", required=True, widget=forms.NumberInput(attrs={'class':'form-control'}))



    def clean_name(self):
        name = self.cleaned_data.get('name')
        event = self.cleaned_data.get('event')
        try:
            area = Area.objects.get(name=self.cleaned_data.get('name'), event=event)
            raise forms.ValidationError('This area name is already in use, please choose a different one.')
        except Area.DoesNotExist:
            pass #username is free

        #or do it this way
    #    users = User.objects.filter(username=self.cleaned_data.get('username'))
    #    if len(users) > 0:
    #        raise forms.ValidationError('This username is already in use, please choose a different one.')

        return name
    def clean(self):
        return self.cleaned_data
