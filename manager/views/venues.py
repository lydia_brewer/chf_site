from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse



@view_function
def process_request(request):
    # ensure user is logged in
    if request.user.has_perm('catalog2.add_venue') or request.user.has_perm('catalog2.change_venue') or request.user.has_perm('catalog2.delete_venue'):

        venues = cmod.Venue.objects.all().order_by('name')

    # send users to users.html
        template_vars = {
                'venues':venues,


          }
        return dmp_render_to_response(request, 'venues.html', template_vars)
    if not request.user.has_permission:
        return HttpResponseRedirect('/homepage/index/')
    else:
        return HttpResponseRedirect('/manager/index/')

@view_function
def delete(request):
    if request.user.has_perm('catalog2.delete_venue'):
        print(request.urlparams[0])
        try:
            venue = cmod.Venue.objects.get(id=request.urlparams[0])
        except cmod.Venue.DoesNotExist:
            return HttpResponseRedirect('/manager/venues/')
        print('time to delete')
        venue.delete()

        return HttpResponseRedirect('/manager/venues/')
