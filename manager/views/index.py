from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from account.models import User


@view_function
def process_request(request):
    # ensure user is logged in
    if not request.user.groups.filter(name="Administrator").exists():
        if not request.user.has_permission:
            return HttpResponseRedirect('/homepage/index/')

    Users = User.objects.filter()

    template_vars = {
        'UserList':Users,

  }
    return dmp_render_to_response(request, 'index.html', template_vars)
