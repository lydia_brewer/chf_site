from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime

from django.http import HttpResponse



@view_function
def process_request(request):
    # ensure user is logged in
    if request.user.has_perm('catalog2.add_event') or request.user.has_perm('catalog2.change_event') or request.user.has_perm('catalog2.delete_event') or request.user.has_perm('catalog2.add_area') or request.user.has_perm('catalog2.change_area') or request.user.has_perm('catalog2.delete_area'):

        events = cmod.Event.objects.all().order_by('name')

    # send users to users.html
        template_vars = {
                'events':events,


          }
        return dmp_render_to_response(request, 'events.html', template_vars)
    if not request.user.has_permission:
        return HttpResponseRedirect('/homepage/index/')
    else:
        return HttpResponseRedirect('/manager/index/')

@view_function
def delete(request):
    if request.user.has_perm('catalog2.delete_venue'):
        print(request.urlparams[0])
        try:
            event = cmod.Event.objects.get(id=request.urlparams[0])
        except cmod.Event.DoesNotExist:
            return HttpResponseRedirect('/manager/events/')
        print('time to delete')
        event.delete()

        return HttpResponseRedirect('/manager/events/')
