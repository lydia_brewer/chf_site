from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from account import models as amod
from django.contrib.auth import authenticate
from django.contrib.auth import login


@view_function
def process_request(request):
    if not request.user.has_perm('account.change_user'):
        if not (request.user.has_perm('account.add_user') or request.user.has_perm('account.delete_user')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/users/')
    try:
        user = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/manager/users/')
    #process the form
    form = ChangeUserPasswordForm()
    if request.method == 'POST':
        form = ChangeUserPasswordForm(request.POST)
        form.user = request.user
        if form.is_valid():
            #create a user object


            user.set_password(form.cleaned_data.get('new_password'))

            user.save()
            # fill the user object with the data from the form
            return HttpResponseRedirect('/homepage/index/')




    template_vars = {
        'form': form,
        'user': user,

  }
    return dmp_render_to_response(request, 'changeuserspassword.html', template_vars)

class ChangeUserPasswordForm(forms.Form):
    new_password = forms.CharField(label='New Password', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    new_password2 = forms.CharField(label='Confirm New Password', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))


    def clean(self):
        if self.cleaned_data.get('new_password') != self.cleaned_data.get('new_password2'):
            raise forms.ValidationError('The two new passwords do not match.')
        return self.cleaned_data
