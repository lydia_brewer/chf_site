from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from catalog2.models import Venue

from catalog2 import models as cmod
from bootstrap3_datetime.widgets import DateTimePicker
from django.forms.models import model_to_dict

@view_function
def process_request(request):
    if not request.user.has_perm('catalog2.change_venue'):
        if not (request.user.has_perm('catalog2.add_venue') or request.user.has_perm('catalog2.delete_venue')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/venues/')
    try:
        v = cmod.Venue.objects.get(id=request.urlparams[0])
    except cmod.Venue.DoesNotExist:
        return HttpResponseRedirect('/manager/venues/')


    #process the form
    form = EditForm(initial={
        'name': v.name,
        'address': v.address,
        'city': v.city,
        'state': v.state,
        'zipcode': v.zipcode,

    })
    if request.method == 'POST':
        form = EditForm(request.POST)

        print("testing form")
        if form.is_valid():
            print("form success")
            v = cmod.Venue.objects.get(id=request.urlparams[0])
            v.name = form.cleaned_data.get('name')
            v.address = form.cleaned_data.get('address')
            v.city = form.cleaned_data.get('city')
            v.state = form.cleaned_data.get('state')
            v.zipcode = form.cleaned_data.get('zipcode')
            v.save()
            return HttpResponseRedirect('/manager/venues/')
            #save data and redirect
        else:
            print("form failure")
            print(form.errors)

            return HttpResponseRedirect('/manager/venues/')


    template_vars = {
            'form':form,
            'venue':v,

      }
    return dmp_render_to_response(request, 'editvenues.html', template_vars)

    #show the edit form html


class EditForm(forms.Form):
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address = forms.CharField(label='*Address', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='*City', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='*State', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='*Zipcode', required=True,max_length=10, widget = forms.TextInput(attrs={'class':'form-control'}))


    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            u3 = Venue.objects.get(name=self.cleaned_data.get('name'))
            if u3.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This name is already in use, please choose a different one.')
        except Venue.DoesNotExist:
            pass #name is free
            return name
        return name

    def clean(self):
        return self.cleaned_data
