from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from catalog2.models import Venue
from bootstrap3_datetime.widgets import DateTimePicker


@view_function
def process_request(request):
    if not request.user.has_perm('catalog2.add_venue'):
        if not (request.user.has_perm('catalog2.change_venue') or request.user.has_perm('catalog2.delete_venue')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index/')
        else:
            return HttpResponseRedirect('/manager/venues/')
    #process the form
    form = CreateVenueForm()

    if request.method == 'POST':
        form = CreateVenueForm(request.POST)
        if form.is_valid():
            #create a user object
            v = Venue()
            v.name = form.cleaned_data.get('name')
            v.address = form.cleaned_data.get('address')
            v.city = form.cleaned_data.get('city')
            v.state = form.cleaned_data.get('state')
            v.zipcode = form.cleaned_data.get('zipcode')
            v.save()

            # fill the user object with the data from the form

            return HttpResponseRedirect('/manager/venues/')




    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'createvenue.html', template_vars)

class CreateVenueForm(forms.Form):
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address = forms.CharField(label='*Address', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='*City', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='*State', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='*Zipcode', required=True,max_length=10, widget = forms.TextInput(attrs={'class':'form-control'}))

    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            venue = Venue.objects.get(name=self.cleaned_data.get('name'))
            raise forms.ValidationError('This venue name is already in use, please choose a different one.')
        except Venue.DoesNotExist:
            pass #username is free

        #or do it this way
    #    users = User.objects.filter(username=self.cleaned_data.get('username'))
    #    if len(users) > 0:
    #        raise forms.ValidationError('This username is already in use, please choose a different one.')

        return name
    def clean(self):
        return self.cleaned_data
