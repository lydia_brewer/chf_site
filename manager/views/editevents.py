from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from catalog2.models import Event

from catalog2 import models as cmod
from bootstrap3_datetime.widgets import DateTimePicker
from django.forms.models import model_to_dict

@view_function
def process_request(request):
    if not request.user.has_perm('catalog2.change_event'):
        if not (request.user.has_perm('catalog2.add_area') or request.user.has_perm('catalog2.change_area') or request.user.has_perm('catalog2.delete_area')):
            if not request.user.has_permission:
                return HttpResponseRedirect('/homepage/index/')
            else:
                return HttpResponseRedirect('/manager/index')
        else:
            try:
                e = cmod.Event.objects.get(id=request.urlparams[0])
            except cmod.Event.DoesNotExist:
                return HttpResponseRedirect('/manager/events/')
            areas = cmod.Area.objects.filter(event = request.urlparams[0]).order_by('name')
            template_vars = {
                    'event':e,
                    'areas':areas,

              }
    else:

        try:
            e = cmod.Event.objects.get(id=request.urlparams[0])
        except cmod.Event.DoesNotExist:
            return HttpResponseRedirect('/manager/events/')

        areas = cmod.Area.objects.filter(event = request.urlparams[0]).order_by('name')



        #process the form
        form = EditEventForm(initial={
            'name': e.name,
            'description': e.description,
            'start_date': e.start_date,
            'end_date': e.end_date,
            'venue': e.venue,

        })
        if request.method == 'POST':
            form = EditEventForm(request.POST)

            print("testing form")
            if form.is_valid():
                print("form success")
                e = cmod.Event.objects.get(id=request.urlparams[0])
                e.name = form.cleaned_data.get('name')
                e.description = form.cleaned_data.get('description')
                e.start_date = form.cleaned_data.get('start_date')
                e.end_date = form.cleaned_data.get('end_date')
                e.venue = form.cleaned_data.get('venue')
                e.save()
                return HttpResponseRedirect('/manager/events/')
                #save data and redirect



        template_vars = {
                'form':form,
                'event':e,
                'areas':areas,

          }
    return dmp_render_to_response(request, 'editevents.html', template_vars)

        #show the edit form html


class EditEventForm(forms.Form):
    name = forms.CharField(label='*Name', required=True, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField(label='*Description', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    start_date = forms.DateField(label="*Start Date", required=True,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    end_date = forms.DateField(label="*End Date", required=True,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    venue = cmod.VenueModelChoiceField(label="*Venue",queryset=cmod.Venue.objects.order_by('name'), required=True, widget = forms.Select(attrs={'class':'form-control'}))


    #birth = forms.DateField(label='Birth',required=False, input_formats=['%Y-%m=%d'])
    # other fields
    def clean_name(self):
        name = self.cleaned_data.get('name')
        try:
            u3 = Event.objects.get(name=self.cleaned_data.get('name'))
            if u3.id == self.cleaned_data.get('id'):
                raise forms.ValidationError('This name is already in use, please choose a different one.')
        except Event.DoesNotExist:
            pass #name is free
            return name
        return name

    def clean(self):
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        if start_date > end_date:
            raise forms.ValidationError('Please choose an End Date that is after the Start Date.')
        return self.cleaned_data

@view_function
def delete(request):
    print(request.urlparams[0])
    return_id = request.urlparams[1]
    try:
        area = cmod.Area.objects.get(id=request.urlparams[0])
    except amod.Area.DoesNotExist:
        return HttpResponseRedirect('/manager/editevents/' + return_id)
    print('time to delete')
    area.delete()

    return HttpResponseRedirect('/manager/editevents/' + return_id)
