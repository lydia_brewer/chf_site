from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime

from django.http import HttpResponse



@view_function
def process_request(request):
    if request.user.has_perm('catalog2.add_product') or request.user.has_perm('catalog2.change_product') or request.user.has_perm('catalog2.delete_product') or request.user.has_perm('catalog2.add_rentable_item') or request.user.has_perm('catalog2.change_rentable_item') or request.user.has_perm('catalog2.delete_rentable_item') or request.user.has_perm('catalog2.add_individual_item') or request.user.has_perm('catalog2.change_individual_item') or request.user.has_perm('catalog2.delete_individual_item') or request.user.has_perm('catalog2.add_bulk_item') or request.user.has_perm('catalog2.change_bulk_item') or request.user.has_perm('catalog2.delete_bulk_item'):

        products = cmod.Product.objects.all().order_by('item_type','name')

        if request.urlparams[0] == "error":
            error = "You cannot edit this type of product."
        else:
            error = ""

    # send products to products.html
        template_vars = {
                'products':products,
                'error':error,


          }
        return dmp_render_to_response(request, 'products.html', template_vars)
    if not request.user.has_permission:
        return HttpResponseRedirect('/homepage/index/')
    else:
        return HttpResponseRedirect('/manager/index/')



@view_function
def delete(request):
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/products/')

    if request.user.has_perm('catalog2.delete_product') or (product.item_type == 'Bulk' and request.user.has_perm('catalog2.delete_bulk_item')) or (product.item_type == 'Individual' and request.user.has_perm('catalog2.delete_individual_item')) or (product.item_type == 'Rental' and request.user.has_perm('catalog2.delete_rentable_item')):

        product.delete()

        return HttpResponseRedirect('/manager/products/')

@view_function
def quantityUpdate(request):
    try:
        product = cmod.Product.objects.get(id = request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/products/')

    return HttpResponse(product.quantity)
