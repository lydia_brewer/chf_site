# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-06 19:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog2', '0007_auto_20160406_1302'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='deleted',
        ),
    ]
