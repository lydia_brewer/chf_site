from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template


#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):



    cart = request.shopping_cart.get_items()
    sale = cmod.Sale.objects.get(id=request.urlparams[0])
    recentproducts = []
    for prodid in request.shopping_cart.get_viewed_items():
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
        print(prodid)
        print(recentproduct)
    cart_quantity = request.shopping_cart.get_item_count()
    address = request.shopping_cart.get_display_sale_address(sale)
    saleitems = cmod.Sale_Item.objects.filter(sale=sale.id)
    template = get_template('receipt2.html')
    context = Context({'address':address,'saleitems':saleitems, 'sale':sale})
    content = template.render(context)
    #if not user.email:
    #    raise BadHeaderError('No email address given for {0}'.format(user))
    print(sale.user.email)
    print(address)
    send_mail('Confirmation for your order', content, settings.EMAIL_HOST_USER,
    [sale.user.email], fail_silently=False,html_message=content)

    template_vars = {
        'cart':cart,
        'recentproducts':recentproducts,
        'cart_quantity':cart_quantity,
        'address':address,
        'sale':sale,
        'saleitems':saleitems,
        }
    return dmp_render_to_response(request, 'receipt.html', template_vars)
