from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime
import stripe
import decimal

from django.http import HttpResponse

#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):
    if request.user.is_authenticated():
        form = PaymentType()
    else:
        form = PaymentType()
    cart = request.shopping_cart.get_items()
    price = 100*request.shopping_cart.calc_total()
    cart_number = request.shopping_cart.get_item_count()
    recentproducts = []
    for prodid in request.shopping_cart.get_viewed_items():
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
        print(prodid)
        print(recentproduct)
    if request.method == "POST":
        form = PaymentType(request.POST)
        if form.is_valid():
            stripe.api_key = "sk_test_BAZGSWUHOE7MEk19im0GE9hC"

            # Get the credit card details submitted by the form
            token = request.POST['stripeToken']
            price = decimal.Decimal(100*request.shopping_cart.calc_total()).quantize(decimal.Decimal('0'))
            print(price)
            print("that was the charge")
            # Create the charge on Stripe's servers - this will charge the user's card
            try:
              charge = stripe.Charge.create(
                  amount=price, # amount in cents, again
                  currency="usd",
                  source=token,
                  description="test charge",
              )
              sale_id = cmod.record_sale(request)
              request.shopping_cart.clear_items()
              return HttpResponseRedirect('/catalog2/receipt/' + str(sale_id))
            except stripe.error.CardError:
                pass
    cart_quantity = request.shopping_cart.get_item_count()
    if request.urlparams[0] == "user":
        address = request.shopping_cart.get_display_address()
    else:
        address = request.shopping_cart.get_display_google_address()
    template_vars = {
        'form':form,
        'cart':cart,
        'recentproducts':recentproducts,
        'cart_number':cart_number,
        'price':price,
        'cart_quantity':cart_quantity,
        'address':address,
        }
    return dmp_render_to_response(request, 'payment.html', template_vars)


class PaymentType(forms.Form):
    OPTIONS = (
       (1, ("Debit Card")),
       (2, ("Credit Card")),
   )
    payment_type = forms.ChoiceField(widget=forms.RadioSelect, choices=OPTIONS)
    def clean(self):
        return self.cleaned_data
