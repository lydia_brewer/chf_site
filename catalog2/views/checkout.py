from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime
import googlemaps
import requests

from django.http import HttpResponse

#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):
    if request.user.is_authenticated():
        form = ShippingAddress(initial={
        'name': request.user.first_name + " " + request.user.last_name,
        'address1' : request.user.address1,
        'address2' : request.user.address2,
        'city' : request.user.city,
        'state' : request.user.state,
        'zipcode' : request.user.zipcode,
        })
    else:
        form = ShippingAddress()
    cart = request.shopping_cart.get_items()
    address = " "
    goog_address = " "
    recentproducts = []
    for prodid in request.shopping_cart.get_viewed_items():
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
        print(prodid)
        print(recentproduct)
    if request.method == "POST":
        form = ShippingAddress(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            address1 = form.cleaned_data.get('address1')
            address2 = form.cleaned_data.get('address2')
            city = form.cleaned_data.get('city')
            state = form.cleaned_data.get('state')
            zipcode = form.cleaned_data.get('zipcode')

            # Make full, concatenated Address
            full_address = address1 + ',+' + city + ',+' + state
            url="https://maps.googleapis.com/maps/api/geocode/json?address=%s" % full_address

            # Get results from google API and format them as JSON
            resp = requests.get(url)
            data = resp.json()

            ###################################################################
            #### Pull useable info out of the JSON response
            # House Number
            google_house_no = data['results'][0]['address_components'][0]['long_name']
            # Street
            google_street = data['results'][0]['address_components'][1]['short_name']
            # Combine two together for street address
            google_address = '%s %s' % (google_house_no, google_street)
            # City
            try:
                google_city = data['results'][0]['address_components'][3]['long_name']
            except google_city.IndexError:
                google_city = data['results'][0]['address_components'][3]['short_name']
            # State
            google_state = data['results'][0]['address_components'][5]['short_name']
            # Zip Code
            try:
                google_zip = data['results'][0]['address_components'][7]['long_name']
            except IndexError:
                google_city = data['results'][0]['address_components'][1]['long_name']
                google_state = data['results'][0]['address_components'][3]['short_name']
                google_zip = data['results'][0]['address_components'][5]['short_name']


            request.shopping_cart.save_address(name, address1, address2, city, state, zipcode)
            request.shopping_cart.save_google_address(name, google_address, address2, google_city, google_state, google_zip)
            address = request.shopping_cart.get_display_address() + "<br><br> <a class=" + '"btn btn-info"' + " href=" + '"/catalog2/payment/user"' + ">Choose me!</a>"
            goog_address = request.shopping_cart.get_display_google_address() + "<br><br> <a class=" + '"btn btn-info"' + " href=" + '"/catalog2/payment/google"' + ">No! Choose me!</a>"
            test_it = google_address + " " + google_city + " " + google_state + " " + google_zip
            print(test_it)




    cart_quantity = request.shopping_cart.get_item_count()
    template_vars = {
        'form':form,
        'cart':cart,
        'recentproducts':recentproducts,
        'address':address,
        'cart_quantity':cart_quantity,
        'goog_address':goog_address,
        }
    return dmp_render_to_response(request, 'checkout.html', template_vars)

class ShippingAddress(forms.Form):
    name = forms.CharField(label='Name', required=False, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address1 = forms.CharField(label='Address 1', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address2 = forms.CharField(label='Address 2', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='City', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='State', required=True,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='Zipcode', required=True,max_length=5, widget = forms.TextInput(attrs={'class':'form-control'}))

    def clean(self):
        return self.cleaned_data
