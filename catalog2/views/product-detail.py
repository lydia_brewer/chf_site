from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime
from CHF.shopping_cart import ShoppingCart

from django.http import HttpResponse

LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):
    product = cmod.Product.objects.get(id=request.urlparams[0])
    request.shopping_cart.item_viewed(product)
    picid = product.id % 4
    print(picid)
    pictures = cmod.ProductImage.objects.filter(product=product.id)
    #main_picture = cmod.ProductImage.objects.get(product=product.id, id=product.id % 4)
    if product.item_type == "Bulk":
        form = PurchaseBulk(initial={
        'quantity':1,
        })
    else:
        form = ""


    recentproducts = []
    for prodid in request.shopping_cart.get_viewed_items():
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
        print(prodid)
        print(recentproduct)
    if product.item_type == "Bulk":
        available_quantity = product.quantity
    else:
        available_quantity = 1
    for item in request.shopping_cart.get_items():
        if item.product_id == product.id:
            if product.item_type == "Individual":
                available_quantity = 0
            else:
                available_quantity = product.quantity - item.quantity
            break
        
    print(recentproducts)
    cart_quantity = request.shopping_cart.get_item_count()
    message = ""
    template_vars = {
            'product':product,
            'form':form,
            'pictures':pictures,
            'recentproducts':recentproducts,
            'cart_quantity':cart_quantity,
            'message':message,
            'available_quantity':available_quantity,
            #'main_picture':main_picture,

        }
    return dmp_render_to_response(request, 'product-detail.html', template_vars)


@view_function
def add_to_cart(request):

    product = cmod.Product.objects.get(id=request.urlparams[0])
    print(product.name)
    if product.item_type == "Bulk":
        form = PurchaseBulk(initial={
        'quantity':1,
        })
        print(request.method)
        print(request.POST)
        if request.method == "POST":
            form = PurchaseBulk(request.POST)
            if form.is_valid():
                print("valid")
                quantity = form.cleaned_data.get('quantity')
                print(quantity)
    else:
        form = ""
        quantity = 1
    #quantity = 1
    #if form.is_valid():
    print(product.name)
    #print(product)
    print(quantity)
    print(request.urlparams[0])
    if request.shopping_cart.check_availability(product, quantity) == True:
        print("product is available!")
        message = "Item added to cart!"
        request.shopping_cart.add_item(product, quantity)
    else:
        message = "There are not enough items available."
        #raise forms.ValidationError("There are not enough items available.")
    print(request.shopping_cart.get_item_count())
    template_vars = {
            'product':product,
            'form':form,
            'cart':request.shopping_cart,
            'message':message,

        }
    return dmp_render_to_response(request, 'product-detail.add_to_cart.html', template_vars)

@view_function
def get_quantity(request):
    return HttpResponse(request.shopping_cart.get_item_count())

@view_function
def get_available_quantity(request):
    product = cmod.Product.objects.get(id=request.urlparams[0])
    print(product.name)
    for item in request.shopping_cart.get_items():
        print("ShoppingCartItem: " + item.name)
        if item.product_id == product.id:
            print("total quantity")
            print(product.quantity)
            print("cart quantity")
            print(item.quantity)
            available_quantity = product.quantity - item.quantity
            break
        else:
            available_quantity = product.quantity
    print(available_quantity)
    return HttpResponse(available_quantity)



class PurchaseBulk(forms.Form):
    quantity = forms.IntegerField(label="", required=False, widget=forms.NumberInput(attrs={'class':'form-control'}))

    def clean(self):
        return self.cleaned_data
