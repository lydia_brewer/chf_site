from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime

from django.http import HttpResponse

#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):

    last5 = request.shopping_cart.get_viewed_items()
    #last5 = []


    products = cmod.Product.objects.all().order_by('name')
    categories = cmod.Category.objects.all().order_by('name')
    form = Search()

    if not request.urlparams[0] == "":
        ctgry = cmod.Category.objects.filter(name=request.urlparams[0])
        products = cmod.Product.objects.filter(category=ctgry)
    recentproducts = []
    for prodid in last5:
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
    print(last5)
    if request.method == 'POST':
        form = Search(request.POST)
        if form.is_valid():
            #products = cmod.Product.objects.filter(name__icontains=form.cleaned_data.get('search'))
            products = products.filter(name__icontains=form.cleaned_data.get('search'))
    cart_quantity = request.shopping_cart.get_item_count()
    template_vars = {
            'products':products,
            'categories':categories,
            'recentproducts':recentproducts,
            'form':form,
            'cart_quantity':cart_quantity,
        }
    return dmp_render_to_response(request, 'products.html', template_vars)


class Search(forms.Form):
    search = forms.CharField(required=False, max_length=100, widget = forms.TextInput(attrs={'placeholder': 'Search Here', 'class':'form-control'}))

    def clean(self):
        return self.cleaned_data
