from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
from django.http import JsonResponse
import requests
from django.http import HttpResponse

#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):
    if request.method == 'GET':
        name = request.GET['name']
        minprice = request.GET['minprice']
        maxprice = request.GET['maxprice']
        category = request.GET['category']
        print(name)
        print(minprice)
        print(maxprice)
        print(category)
        products = cmod.Product.objects.all()
        if not len(name) == 0:
            print("good")
            products = products.filter(name__icontains=name)
        else:
            print("bad")
        if not len(minprice) == 0:
            print("good")
            products = products.filter(price__gte=minprice)
        else:
            print("bad")
        if not len(maxprice) == 0:
            print("good")
            products = products.filter(price__lte=maxprice)
        else:
            print("bad")
        if not len(category) == 0:
            print("good")
            categoryid = cmod.Category.objects.get(name__icontains=category)
            categoryid = categoryid.id

            products = products.filter(category=categoryid)
        else:
            print("bad")

        for product in products:
            print(product.name)
        response_list = []
        for product in products:
            print(product.name)
            response_list.append({
            'name':product.name,
            'price':product.price,
            'category':product.category.name,
            'type':product.item_type,
            'description':product.description,
            'creator':product.creator.username if product.item_type == "Individual" else "",
            'quantity':product.quantity if product.item_type == "Bulk" else 1 if product.item_type == "Individual" and product.available else 0,
            })
        print(response_list)
        return(JsonResponse(response_list, safe=False))
