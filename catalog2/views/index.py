from django.conf import settings
from django import forms
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django_mako_plus.controller import view_function
from .. import dmp_render, dmp_render_to_response
from catalog2 import models as cmod
import datetime

from django.http import HttpResponse

#for five most recently viewed items
LAST_FIVE_KEY = 'pages_viewed'

@view_function
def process_request(request):

    last5 = request.shopping_cart.get_viewed_items()


    recentproducts = []
    for prodid in last5:
        recentproduct = cmod.Product.objects.get(id = prodid.id)
        recentproducts.append(recentproduct)
    print(last5)
    cart_quantity = request.shopping_cart.get_item_count()
    template_vars = {
            'recentproducts':recentproducts,
            'cart_quantity':cart_quantity,
        }
    return dmp_render_to_response(request, 'index.html', template_vars)



@view_function
def quantityUpdate(request):
    try:
        product = cmod.Product.objects.get(id = request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog2/products/')

    return HttpResponse(product.quantity)
