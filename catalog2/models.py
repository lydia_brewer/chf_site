from django.db import models
from django.contrib import admin
from polymorphic.models import PolymorphicModel
from account.models import User
from django.forms import ModelChoiceField
from django.forms import ModelMultipleChoiceField
import time
from datetime import date
# Define models here
class Category(PolymorphicModel):
    name = models.TextField(max_length=100)

class Product(PolymorphicModel):
    name = models.TextField(max_length=100)
    description = models.CharField(max_length=1000)
    image = models.TextField(max_length=1000, default = " ")
    item_type = models.TextField(max_length=100, default = " ")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    

class ProductImage(PolymorphicModel):
    image = models.TextField(max_length=1000)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)


class Individual_Item(Product):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    available = models.BooleanField()

class Bulk_Item(Product):
    quantity = models.PositiveIntegerField()


class Rentable_Item(Product):
    rented_now = models.BooleanField()

class Venue(PolymorphicModel):
    name = models.TextField(max_length=100)
    address = models.TextField(max_length=100)
    city = models.TextField(max_length=100)
    state = models.TextField(max_length=100)
    zipcode = models.TextField(max_length=100)

    def __unicode__(self):
        return u'{0}'.format(self.name)

class Event(PolymorphicModel):
    name = models.TextField(max_length=100)
    description = models.TextField(null=True, blank = True, max_length=100)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)

class Area(PolymorphicModel):
    name = models.TextField(max_length=100)
    description = models.TextField(max_length=100)
    place_number = models.PositiveIntegerField()
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

class VenueModelChoiceField(ModelChoiceField):
    def label_from_instance(self, Venue):
         return Venue.name

class PermissionModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, Permission):
         return Permission.name

class Sale(PolymorphicModel):
    order_date = models.DateField(blank=True)
    ship_date = models.DateField(blank=True, null=True)
    tracking_number = models.PositiveIntegerField( null=True, blank=True)
    total_price = models.DecimalField(decimal_places = 2, max_digits=10)
    ship_name = models.TextField(null=True, blank=True, max_length=100)
    ship_address1 = models.TextField(max_length=100)
    ship_address2 = models.TextField(null=True, blank=True, max_length=100)
    ship_city = models.TextField(max_length=100)
    ship_state = models.TextField(max_length=30)
    ship_zipcode = models.TextField(max_length=12)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Sale_Item(PolymorphicModel):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    description = models.TextField(null=True,blank=True,max_length=500)
    price = models.DecimalField(decimal_places = 2, max_digits=10)
    quantity = models.PositiveIntegerField(null=True, blank=True)
    extended = models.DecimalField(decimal_places = 2, max_digits=10, null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True)

class Payment(PolymorphicModel):
    payment_date = models.DateField(blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    validation_code = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)

def record_sale(request):
    sale = Sale()
    print("Sale Created")
    sale.order_date = date.today()
    print(sale.order_date)
    sale.total_price = request.shopping_cart.calc_total()
    print(sale.total_price)
    i = 0
    if request.urlparams[0] == "user":
        pull_address = request.shopping_cart.address
    else:
        pull_address = request.shopping_cart.google_address
    for item in pull_address:
        if i == 0:
            sale.ship_name = item
            print(sale.ship_name)
        if i == 1:
            sale.ship_address1 = item
            print(sale.ship_address1)
        if i == 2:
            sale.ship_address2 = item
            print(sale.ship_address2)
        if i == 3:
            sale.ship_city = item
            print(sale.ship_city)
        if i == 4:
            sale.ship_state = item
            print(sale.ship_state)
        if i == 5:
            sale.ship_zipcode = item
            print(sale.ship_zipcode)
        i = i + 1
    sale.user = request.user
    print(sale.user.first_name)
    sale.save()
    print("Sale Saved")
    print()

    for item in request.shopping_cart.cart:
        product = Product.objects.get(id=item.product_id)
        if product.item_type == "Individual":
            product.available = False
        elif product.item_type == "Bulk":
            product.quantity = product.quantity - item.quantity
        product.save()
        saleitem = Sale_Item()
        saleitem.description = "Purchased " + str(item.quantity) + " of " + item.name
        saleitem.sale = sale
        saleitem.price = item.price
        saleitem.quantity = item.quantity
        saleitem.extended = item.calc_extended()
        saleitem.product = product
        saleitem.save()
        print("Sale item created for " + product.name)

    taxitem = Sale_Item()
    taxitem.description = "Tax"
    taxitem.sale = sale
    taxitem.price = request.shopping_cart.calc_tax()
    taxitem.quantity = 1
    taxitem.extended = request.shopping_cart.calc_tax()
    taxitem.save()
    print("The tax costs " + str(taxitem.extended))

    shippingitem = Sale_Item()
    shippingitem.description = "Shipping"
    shippingitem.sale = sale
    shippingitem.price = request.shopping_cart.calc_shipping()
    shippingitem.quantity = 1
    shippingitem.extended = request.shopping_cart.calc_shipping()
    shippingitem.save()
    print("The shippping costs " + str(shippingitem.extended))

    payment = Payment()
    payment.payment_date = date.today()
    payment.amount = request.shopping_cart.calc_total()
    payment.user = request.user
    payment.sale = sale
    payment.save()
    print("The customer payed " + str(payment.amount))
    return sale.id
