from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib import admin

# Define models here
class User(AbstractUser):
    address1 = models.TextField(null = True, blank = True)
    address2 = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    birth = models.DateField(null=True, blank=True)
    phone_number = models.TextField(null=True, blank=True)
    has_permission = models.BooleanField(null=False, default=False)

'''def __str__(self):
    return 'User: %s (%s)' % (self.get_full_name(), self.username)

admin.site.register(User)'''
