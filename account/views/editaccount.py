from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from account.models import User
from bootstrap3_datetime.widgets import DateTimePicker

@view_function
def process_request(request):
    # ensure user is logged in
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/account/login/')
    #process the form
    form = EditAccountForm(initial={
      'first_name': request.user.first_name,
      'last_name': request.user.last_name,
      'address1': request.user.address1,
      'address2': request.user.address2,
      'city': request.user.city,
      'state': request.user.state,
      'zipcode': request.user.zipcode,
      'birth': request.user.birth,
      'phone_number': request.user.phone_number,
      'email': request.user.email,
    })
    if request.method == 'POST':
        form = EditAccountForm(request.POST)
        if form.is_valid():
            #create a user object
            u = request.user
            u.first_name = form.cleaned_data.get('first_name')
            u.last_name = form.cleaned_data.get('last_name')
            u.address1 = form.cleaned_data.get('address1')
            u.address2 = form.cleaned_data.get('address2')
            u.city = form.cleaned_data.get('city')
            u.state = form.cleaned_data.get('state')
            u.zipcode = form.cleaned_data.get('zipcode')
            u.birth = form.cleaned_data.get('birth')
            u.phone_number = form.cleaned_data.get('phone_number')
            u.email = form.cleaned_data.get('email')

            u.save()

            # fill the user object with the data from the form

            return HttpResponseRedirect('/account/editaccount/')




    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'editaccount.html', template_vars)

class EditAccountForm(forms.Form):
    first_name = forms.CharField(label='First Name', required=False, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address1 = forms.CharField(label='Address 1', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    address2 = forms.CharField(label='Address 2', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    city = forms.CharField(label='City', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    state = forms.CharField(label='State', required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))
    zipcode = forms.CharField(label='Zipcode', required=False,max_length=10, widget = forms.TextInput(attrs={'class':'form-control'}))
    birth = forms.DateField(label="Birthdate", required=False,input_formats=[ '%Y-%m-%d'],  widget = forms.TextInput(attrs={'placeholder':'YYYY-MM-DD', 'class': 'form-control'}))
    phone_number = forms.CharField(label='Phone Number', required=False,max_length=15, widget = forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label="Email Address", required=False,max_length=100, widget = forms.TextInput(attrs={'class':'form-control'}))

    def clean(self):
        return self.cleaned_data
