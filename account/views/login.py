from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from django.contrib.auth import login
from django.contrib.auth import authenticate
from account.models import User


@view_function
def process_request(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/homepage/index/')
    #process the form

    form = LoginForm()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            #log the user in
            user = form.user
            login(request, form.user)
            #return HttpResponseRedirect('/homepage/index')
            print(form.user.has_permission)
            return HttpResponse('''
            <script>
                window.location.href = '/catalog2/products/';
            </script>
            ''')



    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'login.html', template_vars)

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', required=True, max_length=100, widget = forms.TextInput(attrs={'placeholder': 'Enter Username Here', 'class':'form-control'}))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.PasswordInput(attrs={'placeholder': 'Enter Password Here', 'class':'form-control'}))

    def clean(self):
        user = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password'))
        if user == None:
            raise forms.ValidationError('Sorry!  The username and password entered are not correct.')
        self.user = user

        return self.cleaned_data
