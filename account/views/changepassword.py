from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from account.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login


@view_function
def process_request(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/account/login/')

    #process the form
    form = ChangePasswordForm()
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        form.user = request.user
        if form.is_valid():
            #create a user object
            u = request.user

            u.set_password(form.cleaned_data.get('new_password'))
            u.save()
            u2 = authenticate(username=u.username, password=form.cleaned_data.get('new_password'))
            # fill the user object with the data from the form
            login(request, u2)
            return HttpResponseRedirect('/account/index/')




    template_vars = {
        'form': form,

  }
    return dmp_render_to_response(request, 'changepassword.html', template_vars)

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label='Old Password', required=True, max_length=100, widget = forms.PasswordInput(attrs={'class':'form-control'}))
    new_password = forms.CharField(label='New Password', required=True, max_length=100, widget = forms.PasswordInput(attrs={'class':'form-control'}))
    new_password2 = forms.CharField(label='Confirm New Password', required=True, max_length=100, widget = forms.PasswordInput(attrs={'class':'form-control'}))


    def clean(self):
        if not self.user.check_password(self.cleaned_data.get('old_password')):
            raise forms.ValidationError('The original password you entered is not correct')
        if self.cleaned_data.get('new_password') != self.cleaned_data.get('new_password2'):
            raise forms.ValidationError('The two new passwords do not match.')
        return self.cleaned_data
