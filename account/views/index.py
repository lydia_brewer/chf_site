from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django.http import HttpResponseRedirect
from account.models import User

@view_function
def process_request(request):
    # ensure user is logged in
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/account/login/')
    #process the form

    permissions = request.user.get_all_permissions()
    

    template_vars = {
        'permissions':permissions,

  }
    return dmp_render_to_response(request, 'index.html', template_vars)
