from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse
from .. import dmp_render, dmp_render_to_response
from django import forms
from django.http import HttpResponseRedirect
from django.contrib.auth import logout



@view_function
def process_request(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/account/login/')
    logout(request)

    return HttpResponseRedirect('/homepage/index/')
