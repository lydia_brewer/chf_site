

from catalog2 import models as cmod
import datetime, decimal
from operator import attrgetter

########################################################################
###   Shopping Cart middleware that adds a shopping cart to the
###   current request.
###
###   Installation:
###      1. Since this code uses decimal.Decimal() objects, it cannot use the regular JSON serializer
###         that sessions default to.  Switch to pickle-based sessions by adding this to settings.py:
###
###         SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
###
###      2. Enable this middleware by adding the following to settings.py:
###         (assuming you have this file in the /CHF/ directory, of course)
###
###         MIDDLEWARE_CLASSES = [
###             ...
###             'CHF.shopping_cart.ShoppingCartMiddleware',
###             ...
###         ]
###
###         The above line must come after SessionAuthenticationMiddleware in the list because it uses sessions.
###

# the key we use for the shopping cart list
SHOPPING_CART_KEY = 'shopping_cart'

### STUDENTS ###
# the key we use for the last viewed items
LAST_VIEWED_KEY = 'shopping_cart_last_viewed'
LAST_VIEWED_COUNT = 5  # keep only the last 5

ADDRESS_KEY = 'shipping_address_information'
GOOGLE_ADDRESS_KEY = 'suggested_address_information'

TAX_RATE = decimal.Decimal(0.0725)  # just using a default rate
SHIPPING_AMOUNT = decimal.Decimal(10.00)


class ShoppingCartMiddleware:
    '''Adds shopping cart methods to the request.'''

    def process_request(self, request):
        '''Called when the request starts in Django'''
        # attach a ShoppingCart instance to the request object
        request.shopping_cart = ShoppingCart(request.session)


    def process_response(self, request, response):
        '''Called when the response goes back to the browser.'''
        # save the shopping cart
        request.shopping_cart.save(request.session)

        # return the response per Django docs
        return response



class ShoppingCart(object):
    '''The user's shopping cart.  One of these is created for each request.
       Note that I haven't added any efficiency code yet, such as lazy loading/saving
       the shopping cart object (only when/if it gets accessed) or using a
       set for faster item lookup.  I'm keeping it simple for class purposes.
    '''

    def __init__(self, session):
        '''Constructor.  This is called from process_request() above.'''
        # load the cart from the session
        self.cart = session.get(SHOPPING_CART_KEY, [])
        # load the last 5 items
        self.last5ids = session.get(LAST_VIEWED_KEY, [])

        self.address = session.get(ADDRESS_KEY, [])

        self.google_address = session.get(GOOGLE_ADDRESS_KEY, [])



    def save(self, session):
        '''Saves the current shopping cart to the session.
           This is called from the middleware above.
        '''
        # sort the cart by name
        self.cart.sort(key=attrgetter('name'))
        # set the cart in the session (this saves it to disk so we can access it again next request)
        session[SHOPPING_CART_KEY] = self.cart
        # set the shopping_last_viewed in the session
        session[LAST_VIEWED_KEY] = self.last5ids
        #save the shipping address in the session
        session[ADDRESS_KEY] = self.address
        session[GOOGLE_ADDRESS_KEY] = self.google_address

    ###  SHOPPING CART

    def get_items(self):
        '''Returns the items in the shopping cart'''
        return self.cart


    def check_availability(self, product, desired_quantity=1):
        '''Checks that the product is available at the given quantity.  Raises a ValueError if we don't have enough.'''
        # get the available per the database for Individual or Bulk
        if product.item_type == "Individual":
            available = 1
        elif product.item_type == "Bulk":
            available = product.quantity
        # decrease the available amount by any in our cart
        for item in self.cart:
            if item.product_id == product.id:
                available = available - item.quantity
                break


        # check the available amount and raise ValueError if not enough
        new_quantity = available - desired_quantity
        if new_quantity < 0:
            return False
        else:
            return True

    def add_item(self, product, quantity=1):
        '''Adds the product to the current cart.  If the item already exists in the
           cart, it adds to the quantity of the item.
        '''
        # check the availability
        if self.check_availability(product,quantity):
            done = False
            print('done')
            for item in self.cart:
                if item.product_id == product.id:
                    print()
                    print(item.quantity)
                    print()
                    print(quantity)
                    item.quantity = item.quantity + quantity
                    done = True
                    print('increased item quantity :)')
                    print(item.quantity)

                    break

            # ensure it is in our cart!!!
            if not done:
                newitem = ShoppingItem(product)
                newitem.quantity = decimal.Decimal(quantity)
                self.cart.append(newitem)
                print('added item :)')

            # update the quantity


    def remove_item(self, product):
        '''Removes the given item id from the cart
           The product can be a real product instance or a product id.
        '''
        for item in self.cart:
            if item.product_id == product.id:
                self.cart.remove(item)
                print("item removed from cart")
                break


    def clear_items(self):
        '''Clears all items from the shopping cart'''
        for item in self.cart:
            self.cart.remove(item)


    def get_item_count(self):
        '''Returns the item count'''
        count = 0
        for item in self.cart:
            count = count + item.quantity
        return count


    ###   FINANCIAL METHODS

    def calc_subtotal(self):
        '''Returns the subtotal (sum of product) cost'''
        subtotal = decimal.Decimal(0)
        for item in self.cart:
            subtotal = subtotal + item.calc_extended()
            subtotal = decimal.Decimal(subtotal).quantize(decimal.Decimal('0.01'))
        return subtotal
        pass

    def calc_tax(self):
        '''Returns the tax on the current cart'''

        tax = decimal.Decimal(0)
        tax = self.calc_subtotal()*TAX_RATE
        tax = decimal.Decimal(tax).quantize(decimal.Decimal('0.01'))
        return tax
        pass

    def calc_shipping(self):
        '''Returns the shipping cost on the current cart'''
        shipping = decimal.Decimal(0)
        shipping = decimal.Decimal(SHIPPING_AMOUNT).quantize(decimal.Decimal('0.01'))
        return shipping
        pass

    def calc_total(self):
        '''Returns the total cost on the current cart'''
        total =  self.calc_subtotal() + self.calc_tax() + self.calc_shipping()
        total = decimal.Decimal(total).quantize(decimal.Decimal('0.01'))
        return total

    ###   LAST VIEWED PRODUCT METHODS

    def item_viewed(self, product):
        '''Adds an item to the last-viewed items'''
        # delete from the list if currently there (so it isn't listed twice)
        try:
            self.last5ids.remove(product.id)
        except ValueError:
            pass


        # add the id to the last viewed list
        self.last5ids.insert(0, product.id)

        # ensure the list isn't too long - right now we do 5 items max
        self.last5ids = self.last5ids[:5]


    def get_viewed_items(self):
        '''Returns a Django query object of the last items viewed'''
        # create the list of products from the ids in our last 5 viewed
        product_list = []
        for item in self.last5ids:
            i = cmod.Product.objects.get(id=item)
            product_list.append(i)

        return product_list

    def save_address(self, name, address1, address2, city, state, zipcode):
        self.address = []
        self.address.append(name)
        self.address.append(address1)
        self.address.append(address2)
        self.address.append(city)
        self.address.append(state)
        self.address.append(zipcode)

    def save_google_address(self, name, address1, address2, city, state, zipcode):
        self.google_address = []
        self.google_address.append(name)
        self.google_address.append(address1)
        self.google_address.append(address2)
        self.google_address.append(city)
        self.google_address.append(state)
        self.google_address.append(zipcode)

    def get_address(self):
        address = ""
        for item in self.address:
            if item == " ":
                pass
            else:
                address = address + " " + item
        return address

    def get_google_address(self):
        address = ""
        for item in self.google_address:
            if item == " ":
                pass
            else:
                address = address + " " + item
        return address

    def get_display_sale_address(self, sale):
        if sale.ship_address2 == "":
            address2 = sale.ship_name + "<br>" + sale.ship_address1 + "<br>" + sale.ship_city + ", " + sale.ship_state + " " + sale.ship_zipcode
        else:
            address2 = sale.ship_name + "<br>" + sale.ship_address1 + "<br>" + sale.ship_address2 + "<br>" + sale.ship_city + ", " + sale.ship_state + " " + sale.ship_zipcode
        i = 0
        '''for item in self.address:
            if i == 0:
                address2 = item + " <br>"
            if i == 1:
                address2 = address2 + item + " <br>"
            if i == 2:
                if item == "":
                    pass
                else:
                    address2 = address2 + item + " <br>"
            if i == 3:
                address2 = address2 + item + ", "
            if i == 4:
                address2 = address2 + item + " "
            if i == 5:
                address2 = address2 + item
            i = i + 1'''

        return address2

    def get_display_google_address(self):
        address2 = ""
        i = 0
        for item in self.google_address:
            if i == 0:
                address2 = item + " <br>"
            if i == 1:
                address2 = address2 + item + " <br>"
            if i == 2:
                if item == "":
                    pass
                else:
                    address2 = address2 + item + " <br>"
            if i == 3:
                address2 = address2 + item + ", "
            if i == 4:
                address2 = address2 + item + " "
            if i == 5:
                address2 = address2 + item
            i = i + 1

        return address2

    def get_display_address(self):
        address2 = ""
        i = 0
        for item in self.address:
            if i == 0:
                address2 = item + " <br>"
            if i == 1:
                address2 = address2 + item + " <br>"
            if i == 2:
                if item == "":
                    pass
                else:
                    address2 = address2 + item + " <br>"
            if i == 3:
                address2 = address2 + item + ", "
            if i == 4:
                address2 = address2 + item + " "
            if i == 5:
                address2 = address2 + item
            i = i + 1

        return address2





class ShoppingItem(object):
    '''A shopping cart item. Each of these objects represents a product in the shopping cart.  In
       other words, the shopping cart is a list of these objects (ShoppingItem objects).

       This class cannot contain any pointers because it is serialized to the user's session object.
       That's why it references product.id instead of product directly.
    '''
    def __init__(self, product):
        '''Constructor.  Starts the quantity at 0, so be sure to set it.'''
        self.product_id = product.id
        #self.filename = product.get_image_filename()
        self.name = product.name
        self.price = product.price
        self.image = product.image
        self.quantity = decimal.Decimal(0)


    def calc_extended(self):
        return self.price * self.quantity
